===========================
Stock Shipment Out Scenario
===========================

Imports::

    >>> import datetime
    >>> import re
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> yesterday = today - relativedelta(days=1)

Install stock Module::

    >>> config = activate_modules('party_shipment_out_sequence')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> config._context = User.get_preferences(True, config.context)

Configure sequences::

    >>> Configuration = Model.get('stock.configuration')
    >>> Sequence = Model.get('ir.sequence')
    >>> seq1 = Configuration(1).shipment_out_sequence
    >>> seq1.prefix = 'SS'
    >>> seq1.save()
    >>> new_seq = Sequence(name='Shipment out cust. 2',
    ...     sequence_type=seq1.sequence_type, company=company)
    >>> new_seq.prefix = 'SO'
    >>> new_seq.save()

Create customers::

    >>> Party = Model.get('party.party')
    >>> customer1 = Party(name='Customer 1')
    >>> customer1.save()
    >>> customer2 = Party(name='Customer 2')
    >>> customer2.shipment_out_sequence = new_seq
    >>> customer2.save()

Create category::

    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.categories.append(category)
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.cost_price = Decimal('8')
    >>> template.save()
    >>> product.template = template
    >>> product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])

Create Shipment Out::

    >>> ShipmentOut = Model.get('stock.shipment.out')
    >>> shipment_out = ShipmentOut()
    >>> shipment_out.planned_date = today
    >>> shipment_out.customer = customer1
    >>> shipment_out.warehouse = warehouse_loc
    >>> shipment_out.company = company

Add two shipment lines of same product::

    >>> StockMove = Model.get('stock.move')
    >>> shipment_out.outgoing_moves.extend([StockMove(), StockMove()])
    >>> for move in shipment_out.outgoing_moves:
    ...     move.product = product
    ...     move.uom =unit
    ...     move.quantity = 1
    ...     move.from_location = output_loc
    ...     move.to_location = customer_loc
    ...     move.company = company
    ...     move.unit_price = Decimal('1')
    ...     move.currency = company.currency
    >>> shipment_out.save()

Check sequence::

    >>> p = re.compile('SS*')
    >>> not p.match(shipment_out.number)
    False
    >>> shipment_out.number
    'SS1'

Create Shipment Out with effective date::

    >>> shipment_out = ShipmentOut()
    >>> shipment_out.planned_date = yesterday
    >>> shipment_out.effective_date = yesterday
    >>> shipment_out.customer = customer2
    >>> shipment_out.warehouse = warehouse_loc
    >>> shipment_out.company = company
    >>> move = shipment_out.outgoing_moves.new()
    >>> move.product = product
    >>> move.uom =unit
    >>> move.quantity = 1
    >>> move.from_location = output_loc
    >>> move.to_location = customer_loc
    >>> move.company = company
    >>> move.unit_price = Decimal('1')
    >>> move.currency = company.currency
    >>> shipment_out.save()

Check sequence::

    >>> p = re.compile('SO*')
    >>> not p.match(shipment_out.number)
    False
    >>> shipment_out.number
    'SO1'
    >>> seq1.reload()
    >>> int(seq1.number_next)
    2

Create Shipment Out::

    >>> shipment_out = ShipmentOut()
    >>> shipment_out.planned_date = today
    >>> shipment_out.customer = customer1
    >>> shipment_out.warehouse = warehouse_loc
    >>> shipment_out.company = company
    >>> shipment_out.sequence == seq1
    True
    >>> shipment_out.save()
    >>> shipment_out.number
    'SS2'

Create Shipment Out changing sequence::

    >>> shipment_out = ShipmentOut()
    >>> shipment_out.planned_date = today
    >>> shipment_out.customer = customer1
    >>> shipment_out.warehouse = warehouse_loc
    >>> shipment_out.company = company
    >>> shipment_out.sequence = new_seq
    >>> shipment_out.save()
    >>> shipment_out.number
    'SO2'